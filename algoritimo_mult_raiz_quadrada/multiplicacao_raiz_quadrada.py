import numpy as np
import sys

# Define a matriz A com números aleatórios
a = np.random.randint(1, 10, size=(4, 4))

# Define a matriz B com números aleatórios
b = np.random.randint(1, 10, size=(4, 4))

# Imprime a matriz A
print("Matriz A:")
np.savetxt(sys.stdout, a, fmt='%5d')

# Imprime a matriz B
print("\nMatriz B:")
np.savetxt(sys.stdout, b, fmt='%5d')

# Inicializa uma matriz vazia do mesmo tamanho das matrizes A e B
produto = np.zeros((4, 4))

# Loop para iterar sobre as linhas da matriz A
for i in range(len(a)):
    
    # Loop para iterar sobre as colunas da matriz B
    for j in range(len(b[0])):
        
        # Loop para armazenar os valores dos cálculos das linhas de A
        # com as colunas de B
        for k in range(len(b)):
            produto[i][j] += a[i][k] * b[k][j]

# Cria uma matriz a partir do resultado
produto_numpy = np.matrix(produto)

# Imprime a matriz resultado alinhada
print("\nMatriz Produto:")
np.savetxt(sys.stdout, produto_numpy, fmt='%5d')
